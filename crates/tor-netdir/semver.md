MODIFIED: Exposed addrs_in_same_subnets() api from SubnetConfig
ADDED: DirEvent is now repr(u16) and implements several additional enum traits
